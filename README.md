# Proyectito

Es una aplicación para determinar tu personalidad según un antiguo psicologo llamado Rorschach.


## Requerimientos

  * Android Studio
  * Gradle

## Uso

Para probar este ejemplo clona este repositorio de la siguiente forma:

>     $ git clone https://gitlab.com/donEber/rorschach.git

o puedes descargarlo en .zip y descomprimirlo

Dentro de Android Studio:

* File --> New --> Import Project
* Seleccionas la ruta donde hiciste el `clone` o descomprimiste el proyecto.
* Build --> Rebuild Project
* Run

