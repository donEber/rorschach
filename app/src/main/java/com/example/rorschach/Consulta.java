package com.example.rorschach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class Consulta extends AppCompatActivity {
    String idPaciente;
    Button botonVolver, botonSiguiente, botonAnterior;
    Spinner spinnerOpciones;
    ImageView imagenesView;
    int puntero = 0;
    int[] imagenes = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};
    FirebaseFirestore db;
    String vectorRespuestas[]=new String[10];

    String array1[]={"uno"};
    String array2[]={"uno","dos"};
    String array3[]={"uno","dos","tres"};
    String array4[]={"uno","dos","tres"};
    String array5[]={"uno","dos"};
    String array6[]={"uno","dos"};
    String array7[]={"uno","dos"};
    String array8[]={"uno","dos"};
    String array9[]={"uno","dos"};
    String array10[]={"uno","dos"};
    String[][] arrays = new String[][] { array1, array2, array3, array4, array5, array6,array7,array8,array9,array10};

    //String nombrePaciente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);
        //Obtenemos los datos
        Bundle miBundle = this.getIntent().getExtras();
        idPaciente = miBundle.getString("idPaciente");
        //nombrePaciente = miBundle.getString("nombrePaciente");
        //
        botonVolver = findViewById(R.id.buttonVolver);
        botonVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //FIREBASE
        db = FirebaseFirestore.getInstance();
        mostrarDatosSpinner();
        imagenesView = findViewById(R.id.imageRorscharch);
        //Boton siguiente
        botonSiguiente = findViewById(R.id.buttonSiguiente);
        botonSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntero++;
                if (puntero < 10) {
                    mostrarDatosSpinner();
                    imagenesView.setImageResource(imagenes[puntero]);

                }
                if (puntero == 9) {
                    botonSiguiente.setText("Terminar");
                }
                if (puntero == 10) {
                    guardarUsuario();//Guarda los datos en la base de datos
                    Bundle datos = new Bundle();
                    datos.putStringArray("vectorRespuestas", vectorRespuestas);
                    //datos.putString("nombrePaciente",nombrePaciente);
                    Intent vistaResultado = new Intent(Consulta.this, Resultado.class);
                    vistaResultado.putExtras(datos);
                    startActivity(vistaResultado);
                    finish();
                }


            }
        });

        //Boton anterior
        botonAnterior = findViewById(R.id.buttonAnterior);
        botonAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (puntero > 0) {
                    puntero--;
                    mostrarDatosSpinner();
                    imagenesView.setImageResource(imagenes[puntero]);
                }
                if (puntero < 9) {
                    botonSiguiente.setText("Siguiente");
                }
            }
        });
    }

    private void mostrarDatosSpinner() {
        //Spinner (drop down)
        spinnerOpciones = findViewById(R.id.spinner);
        String[] opciones = arrays[puntero];
        ArrayAdapter miAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, opciones);
        spinnerOpciones.setAdapter(miAdapter);
        spinnerOpciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("RESPUESTA",position+"");
                vectorRespuestas[puntero]=position+"";
                Log.d("RESPUESTA","Respuesta: "+ vectorRespuestas[puntero]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void guardarUsuario() {
        // Create a new pacient with a first, middle, and last name
        Map<String, Object> respuestas = new HashMap<>();
        respuestas.put("uno", this.vectorRespuestas[0]+"");
        respuestas.put("dos", this.vectorRespuestas[1]+"");
        respuestas.put("tres", this.vectorRespuestas[2]+"");
        respuestas.put("cuatro", this.vectorRespuestas[3]+"");
        respuestas.put("cinco", this.vectorRespuestas[4]+"");
        respuestas.put("seis", this.vectorRespuestas[5]+"");
        respuestas.put("siete", this.vectorRespuestas[6]+"");
        respuestas.put("ocho", this.vectorRespuestas[7]+"");
        respuestas.put("nueve", this.vectorRespuestas[8]+"");
        respuestas.put("diez", this.vectorRespuestas[9]+"");

        // Registra un nuevo paciente asociado al usuario loggeado
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String emailActual = currentUser.getEmail();
            SimpleDateFormat sdf =new SimpleDateFormat("dd-MM-yyyy \t\t\t\t\t\t\t\t\t\t\t\thh:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT-3"));
            String date = sdf.format(new Date());
            db.collection("usuarios").document(emailActual)
                    .collection("pacientes").
                    document(idPaciente)
                    .collection("historialRespuestas")
                    .document(date).set(respuestas);
        }

    }
}
