package com.example.rorschach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class EditarPaciente extends AppCompatActivity {
    Button botonCancelar, botonGuardar;
    String nombrePaciente, edadPaciente, idPaciente, emailUsuario;
    EditText nombreEditado, edadEditado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_paciente);
        //recibe datos de bundle
        Bundle miBundle = this.getIntent().getExtras();
        nombrePaciente = miBundle.getString("nombrePaciente");
        edadPaciente = miBundle.getString("edadPaciente");
        idPaciente = miBundle.getString("idPaciente");
        emailUsuario = miBundle.getString("emailUsuario");
        Log.d("EDITAR", "Datos editar son:" + nombrePaciente + edadPaciente + " ID: " + idPaciente);
        //muestra datos
        nombreEditado = findViewById(R.id.editTextNombre);
        nombreEditado.setText(nombrePaciente);
        edadEditado = findViewById(R.id.editTextEdad);
        edadEditado.setText(edadPaciente);
        //Btn cancelar
        botonCancelar = findViewById(R.id.buttonCancelar);
        botonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle datos = new Bundle();
                datos.putString("id",idPaciente);
                datos.putString("emailUsuario",emailUsuario);
                Intent vistaPerfilPaciente = new Intent(EditarPaciente.this,PerfilPaciente.class);
                vistaPerfilPaciente.putExtras(datos);
                startActivity(vistaPerfilPaciente);
                finish();
            }
        });
        //Btn guardar
        botonGuardar = findViewById(R.id.buttonGuardar);
        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> paciente = new HashMap<>();
                nombrePaciente = nombreEditado.getText().toString();
                edadPaciente = edadEditado.getText().toString();
                paciente.put("nombre", nombrePaciente);
                paciente.put("edad", edadPaciente);
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                db.collection("usuarios")
                        .document(emailUsuario)
                        .collection("pacientes")
                        .document(idPaciente)
                        .update(paciente).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("EDITAR", "DocumentSnapshot successfully updated!");
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("EDITAR", "Error updating document", e);
                            }
                        });
                Bundle datos = new Bundle();
                datos.putString("id",idPaciente);
                datos.putString("emailUsuario",emailUsuario);
                Intent vistaPerfilPaciente = new Intent(EditarPaciente.this,PerfilPaciente.class);
                vistaPerfilPaciente.putExtras(datos);
                startActivity(vistaPerfilPaciente);
                finish();
            }
        });
    }
}
