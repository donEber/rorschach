package com.example.rorschach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class HistorialPaciente extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView listaHistorial;
    Button volver;
    TextView titulo;
    String idPaciente, nombrePaciente;
    List<String> historial;
    List<String> IDsHistorial;
    List<String[]> listaRespuestasPacientes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_paciente);
        //Obtenemos los datos
        Bundle miBundle = this.getIntent().getExtras();
        idPaciente = miBundle.getString("idPaciente");
        nombrePaciente = miBundle.getString("nombrePaciente");
        titulo = findViewById(R.id.textViewTitulo);
        titulo.setText("Historial de "+nombrePaciente);
        //Inicializamos
        historial = new ArrayList<>();
        IDsHistorial = new ArrayList<>();
        listaRespuestasPacientes = new ArrayList<>();
        //Btn volver
        volver = findViewById(R.id.buttonVolver);
        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String emailActual = currentUser.getEmail();

        db.collection("usuarios")
                .document(emailActual)
                .collection("pacientes")
                .document(idPaciente)
                .collection("historialRespuestas")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()){
                                Log.d("LISTA", document.getId() + " => " + document.getData());
                                historial.add(document.getId());
                                IDsHistorial.add(document.getId());
                                String[] respuestasPaciente = {
                                        document.getString("uno"),
                                        document.getString("dos"),
                                        document.getString("tres"),
                                        document.getString("cuatro"),
                                        document.getString("cinco"),
                                        document.getString("seis"),
                                        document.getString("siete"),
                                        document.getString("ocho"),
                                        document.getString("nueva"),
                                        document.getString("diez"),
                                };
                                listaRespuestasPacientes.add(respuestasPaciente);
                            }
                            ArrayAdapter adaptador = new ArrayAdapter(HistorialPaciente.this,android.R.layout.simple_list_item_1,historial);
                            listaHistorial = findViewById(R.id.listViewHistorial);
                            listaHistorial.setAdapter(adaptador);
                            listaHistorial.setOnItemClickListener(HistorialPaciente.this);
                        } else {
                            Log.d("LISTA", "Error getting documents: ", task.getException());
                        }
                    }
                });
        //

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent vistaResultado = new Intent(HistorialPaciente.this,Resultado.class);
        Bundle datos = new Bundle();
        datos.putString("idPaciente",idPaciente);
        datos.putString("idHistorial",IDsHistorial.get(position));
        datos.putStringArray("vectorRespuestas",listaRespuestasPacientes.get(position));
        vistaResultado.putExtras(datos);
        startActivity(vistaResultado);
    }
}
