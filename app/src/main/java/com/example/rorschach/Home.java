package com.example.rorschach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class Home extends AppCompatActivity {
    private FirebaseAuth mAuth;
    Button botonNuevaConsulta, botonListarPacientes, botonSalir, botonCerrarSesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_home);
        botonNuevaConsulta = findViewById(R.id.buttonNuevaConsulta);
        botonNuevaConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevaConsulta = new Intent(v.getContext(), NuevoPaciente.class);
                startActivity(nuevaConsulta);
            }
        });

        botonListarPacientes = findViewById(R.id.buttonListarPacientes);
        botonListarPacientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listarPacientes = new Intent(v.getContext(),ListaPacientes.class);
                startActivity(listarPacientes);
            }
        });
        //Boton Salir
        botonSalir = findViewById(R.id.buttonSalir);
        botonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Boton Cerrar Sesion
        botonCerrarSesion = findViewById(R.id.buttonCerrarSesion);
        botonCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                startActivity(new Intent(Home.this, Login.class));
                finish();
            }
        });
    }
}
