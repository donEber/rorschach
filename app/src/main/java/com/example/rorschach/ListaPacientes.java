package com.example.rorschach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ListaPacientes extends AppCompatActivity implements AdapterView.OnItemClickListener {
    Button botonVolver;
    ListView listaPacientes;
    List<String> nombrePacientes;
    List<String> edadPacientes;
    List<String> idPacientes;
    String emailUsuario;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacientes);
        //inicializamos vectores
        nombrePacientes = new ArrayList<>();
        edadPacientes = new ArrayList<>();
        idPacientes = new ArrayList<>();
        //
        db = FirebaseFirestore.getInstance();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            //Obtenemos los datos del usuario actual
            emailUsuario = currentUser.getEmail();
            //OBTENEMOS LOS PACIENTES DEL USUARIO ACTUAL
            db.collection("usuarios")
                    .document(emailUsuario)
                    .collection("pacientes")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d("LISTA", document.getId() + " => " + document.getData());
                                    String elNombre = document.getString("nombre");
                                    String laEdad = document.getString("edad");
                                    String elId = document.getId();
                                    nombrePacientes.add(elNombre);
                                    edadPacientes.add(laEdad);
                                    idPacientes.add(elId);
                                }
                                ArrayAdapter adapter = new ArrayAdapter(ListaPacientes.this, android.R.layout.simple_list_item_1, nombrePacientes);
                                // acciones que se ejecutan tras los milisegundos
                                listaPacientes = findViewById(R.id.listViewPacientes);
                                listaPacientes.setAdapter(adapter);
                                //Al hacer click
                                listaPacientes.setOnItemClickListener(ListaPacientes.this);
                            } else {
                                Log.d("LISTA", "Error getting documents: ", task.getException());
                            }
                        }
                    });
        }
        //
        botonVolver = findViewById(R.id.buttonVolver);
        botonVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle datosPaciente = new Bundle();
        datosPaciente.putString("id", idPacientes.get(position));
        datosPaciente.putString("emailUsuario", emailUsuario);
        //datosPaciente.putStringArray("vectorRespuestas",respuestasPacientes.get(position));
        Intent pacienteVista = new Intent(view.getContext(), PerfilPaciente.class);
        pacienteVista.putExtras(datosPaciente);
        startActivity(pacienteVista);
        finish();
    }
}
