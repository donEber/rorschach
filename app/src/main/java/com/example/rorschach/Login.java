package com.example.rorschach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    Button botonLogin, botonRegistrarse;

    // Paso 1
    private EditText editTextUsuario;
    private EditText editTextClave;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //ocultamos el toolbar
        getSupportActionBar().hide();
        // Iniciamos la autenticacion de Firebase
        mAuth = FirebaseAuth.getInstance();
        editTextUsuario = findViewById(R.id.editTextUsuario);
        editTextClave = findViewById(R.id.editTextPassword);

        //Boton para INGRESAR a la aplicacion
        botonLogin = findViewById(R.id.buttonIngresar);
        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuarioEmail = editTextUsuario.getText().toString().trim();
                String usuarioClave = editTextClave.getText().toString().trim();
                if(validarUsuario(usuarioEmail,usuarioClave)){
                    iniciarSesion(usuarioEmail,usuarioClave);
                }
                //finish();
            }
        });
        //Boton para REGISTRARSE
        botonRegistrarse = findViewById(R.id.buttonRegistrar);
        botonRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuarioEmail = editTextUsuario.getText().toString().trim();
                String usuarioClave = editTextClave.getText().toString().trim();
                if(validarUsuario(usuarioEmail,usuarioClave))
                    registrarUsuario(usuarioEmail, usuarioClave);
            }
        });
    }
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            startActivity(new Intent(Login.this, Home.class));
            finish();
        }

    }
    public boolean validarUsuario(String usuarioEmail, String usuarioClave) {

        if (TextUtils.isEmpty(usuarioEmail)) {
            Toast.makeText(Login.this, "Ingrese email, joven!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(usuarioClave)) {
            Toast.makeText(Login.this, "Ingrese clave pe, que pasa!", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(usuarioClave.length()<6){
            Toast.makeText(Login.this, "Clave mas larga plox", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void registrarUsuario(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            actualizarVista(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(Login.this, "Fallo la autenticacion.",
                                    Toast.LENGTH_SHORT).show();
                            actualizarVista(null);
                        }
                    }
                });
    }
    public void iniciarSesion(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            actualizarVista(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("LOGIN", "signInWithEmail:failure", task.getException());
                            Toast.makeText(Login.this, "Fallo la autenticacion.",
                                    Toast.LENGTH_SHORT).show();
                            actualizarVista(null);
                        }

                        // ...
                    }
                });
    }

    private void actualizarVista(FirebaseUser user) {
        //Verificamos si existe el usuario
        if(user!=null){
            startActivity(new Intent(Login.this, Home.class));
            finish();
        }

    }
}
