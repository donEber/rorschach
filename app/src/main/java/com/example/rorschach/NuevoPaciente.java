package com.example.rorschach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class NuevoPaciente extends AppCompatActivity {
    Button guardar, botonCancelar;
    EditText etNombre, etEdad;
    FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_paciente);
        db = FirebaseFirestore.getInstance();
        guardar = findViewById(R.id.buttonComenzar);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //datos
                etNombre = findViewById(R.id.editTextNombre);
                etEdad = findViewById(R.id.editTextEdad);
                String nombre = etNombre.getText().toString();
                String edad = etEdad.getText().toString();
                //bundle
                Bundle miBundle = new Bundle();
                miBundle.putString("nombre",nombre);
                miBundle.putString("edad",edad);

                // Registra un nuevo paciente asociado al usuario loggeado
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                Map<String, Object> pacient = new HashMap<>();
                pacient.put("nombre", nombre);
                pacient.put("edad", edad);
                String emailUsuario = currentUser.getEmail();
                String elId =UUID.randomUUID().toString();
                db.collection("usuarios")
                        .document(emailUsuario)
                        .collection("pacientes")
                        .document(elId)
                        .set(pacient);
                miBundle.putString("id",elId);
                miBundle.putString("emailUsuario",emailUsuario);
                //intent
                Intent consulta = new Intent(v.getContext(), PerfilPaciente.class);
                consulta.putExtras(miBundle);
                startActivity(consulta);
                finish();
            }
        });
        botonCancelar = findViewById(R.id.buttonCancelar);
        botonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
