package com.example.rorschach;

public class Paciente {
    private String nombre;
    private String edad;
    private String uno;
    private String dos;
    private String tres;
    private String cuatro;
    private String cinco;
    private String seis;
    private String siete;
    private String ocho;
    private String nueve;
    private String diez;

    public Paciente(String nombre, String edad, String uno, String dos, String tres, String cuatro, String cinco, String seis, String siete, String ocho, String nueve, String diez) {
        this.nombre = nombre;
        this.edad = edad;
        this.uno = uno;
        this.dos = dos;
        this.tres = tres;
        this.cuatro = cuatro;
        this.cinco = cinco;
        this.seis = seis;
        this.siete = siete;
        this.ocho = ocho;
        this.nueve = nueve;
        this.diez = diez;
    }

    public Paciente() {
    }

    public Paciente(String nombre, String edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }
}
