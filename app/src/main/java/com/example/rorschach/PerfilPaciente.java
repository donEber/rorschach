package com.example.rorschach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class PerfilPaciente extends AppCompatActivity {
    Button botonVolver, botonEditar, botonHistorial, botonNuevaConsulta, botonEliminar;
    String idPaciente, nombre_recibido, edad_recibido, emailUsuario;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_paciente);
        //recibirDatos (bundle)
        Bundle miBundle = this.getIntent().getExtras();
        idPaciente = miBundle.getString("id");
        emailUsuario = miBundle.getString("emailUsuario");
        //obtener datos db (firestore)
        db = FirebaseFirestore.getInstance();
        db.collection("usuarios")
                .document(emailUsuario)
                .collection("pacientes")
                .document(idPaciente)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        //Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        nombre_recibido = document.getString("nombre");
                        edad_recibido = document.getString("edad");
                        //actualizando datos
                        ((TextView) findViewById(R.id.textViewNombre)).setText(nombre_recibido);
                        ((TextView) findViewById(R.id.textViewEdad)).setText(edad_recibido);
                    } else {
                        Log.d("USUARIO", "No such document");
                    }
                } else {
                    Log.d("USUARIO", "get failed with ", task.getException());
                }
            }
        });
        botonVolver = findViewById(R.id.buttonVolver);
        botonVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PerfilPaciente.this,ListaPacientes.class));
                finish();
            }
        });
        botonNuevaConsulta = findViewById(R.id.buttonNuevaConsulta);
        botonNuevaConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle datos = new Bundle();
                datos.putString("idPaciente", idPaciente);
                //datos.putString("nombrePaciente",nombre_recibido);
                Intent consultaActivity = new Intent(v.getContext(), Consulta.class);
                consultaActivity.putExtras(datos);
                startActivity(consultaActivity);
            }
        });
        //Boton edita
        botonEditar = findViewById(R.id.buttonEditar);
        botonEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle datos = new Bundle();
                datos.putString("idPaciente", idPaciente);
                datos.putString("nombrePaciente", nombre_recibido);
                datos.putString("edadPaciente", edad_recibido);
                datos.putString("emailUsuario", emailUsuario);
                Intent editarPaciente = new Intent(v.getContext(), EditarPaciente.class);
                editarPaciente.putExtras(datos);
                startActivity(editarPaciente);
                finish();
            }
        });

        //Boton muestra Resultado
        botonHistorial = findViewById(R.id.buttonResultado);
        botonHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle datos = new Bundle();
                datos.putString("idPaciente", idPaciente);
                datos.putString("nombrePaciente", nombre_recibido);
                Intent historialActivity = new Intent(v.getContext(), HistorialPaciente.class);
                historialActivity.putExtras(datos);
                startActivity(historialActivity);
            }
        });
        //Boton eliminar
        botonEliminar = findViewById(R.id.buttonEliminar);
        botonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.collection("usuarios")
                        .document(emailUsuario)
                        .collection("pacientes")
                        .document(idPaciente)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("ELIMINAR", "DocumentSnapshot successfully deleted!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("ELIMINAR", "Error deleting document", e);
                            }
                        });
                startActivity(new Intent(PerfilPaciente.this,ListaPacientes.class));
                finish();
            }
        });
    }
}
