package com.example.rorschach;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Resultado extends AppCompatActivity {
    Button botonVolver;
    TextView contenido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        Bundle miBundle = this.getIntent().getExtras();
        String[] vectorRespuestas_recibido = miBundle.getStringArray("vectorRespuestas");
        String reses="\n\n";
        for (String respuesta : vectorRespuestas_recibido){
            reses+=respuesta+"\t";
        }

        //contenido
        contenido = findViewById(R.id.textViewContenido);
        contenido.setText(contenido.getText()+reses);


        //btn volver
        botonVolver = findViewById(R.id.buttonVolver);
        botonVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
